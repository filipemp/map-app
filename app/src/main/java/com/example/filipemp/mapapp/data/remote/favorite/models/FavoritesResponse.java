package com.example.filipemp.mapapp.data.remote.favorite.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by filipe.mp on 8/15/17.
 */

public class FavoritesResponse {
    @SerializedName("favorites")
    @Expose
    private List<FavoriteResponse> favoriteResponses = null;

    public List<FavoriteResponse> getFavoriteResponses() {
        return favoriteResponses;
    }

    public void setFavoriteResponses(List<FavoriteResponse> favoriteResponses) {
        this.favoriteResponses = favoriteResponses;
    }
}
