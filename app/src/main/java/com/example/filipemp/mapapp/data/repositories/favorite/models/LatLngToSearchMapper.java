package com.example.filipemp.mapapp.data.repositories.favorite.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by filipe.mp on 8/16/17.
 */

public class LatLngToSearchMapper {

    public static SearchResult map(LatLng latLng, String address) {
        String name = address != null ? address : "";
        return new SearchResult(name, latLng.latitude, latLng.longitude, false);
    }
}
