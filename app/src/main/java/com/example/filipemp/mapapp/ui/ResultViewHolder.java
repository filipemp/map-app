package com.example.filipemp.mapapp.ui;

import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.filipemp.mapapp.R;
import com.example.filipemp.mapapp.data.repositories.favorite.models.SearchResult;

/**
 * Created by filipe.mp on 8/16/17.
 */

public class ResultViewHolder extends RecyclerView.ViewHolder {

    public interface ViewHolderListener {
        void onStarClicked(int position, ResultViewHolder viewHolder);
        void onItemClicked(int position);
    }

    private TextView name;
    private RatingBar favoriteStar;
    private int position;

    private ViewHolderListener listener;

    public ResultViewHolder(View itemView, ViewHolderListener listener) {
        super(itemView);

        this.listener = listener;

        name = (TextView) itemView.findViewById(R.id.result_name);
        favoriteStar = (RatingBar) itemView.findViewById(R.id.favorite_star);

        favoriteStar.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                listener.onStarClicked(position, ResultViewHolder.this);
            }
            return true;
        });

        itemView.setOnClickListener(v -> listener.onItemClicked(position));
    }

    public void updateStarState() {
        if (favoriteStar.getRating() > 0) {
            favoriteStar.setRating(0.0f);
        } else {
            favoriteStar.setRating(1.0f);
        }
    }

    public void bind(SearchResult result, int position) {
        this.position = position;

        name.setText(result.getName());
        favoriteStar.setRating(result.isFavorite() ? 1 : 0);
    }
}
