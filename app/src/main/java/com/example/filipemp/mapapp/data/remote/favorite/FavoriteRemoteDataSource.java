package com.example.filipemp.mapapp.data.remote.favorite;

import com.example.filipemp.mapapp.data.remote.commons.BaseRemoteDataSource;
import com.example.filipemp.mapapp.data.remote.commons.RetrofitClient;
import com.example.filipemp.mapapp.data.remote.favorite.models.FavoriteResponse;
import com.example.filipemp.mapapp.data.remote.favorite.models.FavoritesResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by filipe.mp on 8/15/17.
 */

public class FavoriteRemoteDataSource extends BaseRemoteDataSource {

    private static final String BASE_URL = "http://private-830fa5-esdrasdl.apiary-mock.com/";

    private FavoriteService service;

    public interface FavoriteListener {
        void onSuccess(List<FavoriteResponse> favorites);
    }

    private FavoriteListener favoriteListener;

    public FavoriteRemoteDataSource(FailureListener failureListener, FavoriteListener favoriteListener) {
        super(failureListener);
        service = RetrofitClient.getClient(BASE_URL).create(FavoriteService.class);
        this.favoriteListener = favoriteListener;
    }

    public void getFavorites() {
        service.getFavorites().enqueue(new Callback<FavoritesResponse>() {
            @Override
            public void onResponse(Call<FavoritesResponse> call, Response<FavoritesResponse> response) {
                if (response.isSuccessful() && response.body().getFavoriteResponses() != null) {
                    favoriteListener.onSuccess(response.body().getFavoriteResponses());
                } else {
                    failureListener.onFailure("Tivemos algum problema com nosso servidor. " +
                            "Os Oompa-Lompas já estão checando");
                }
            }

            @Override
            public void onFailure(Call<FavoritesResponse> call, Throwable t) {
                failureListener.onFailure("Verique sua conexão, por favor");
            }
        });
    }
}
