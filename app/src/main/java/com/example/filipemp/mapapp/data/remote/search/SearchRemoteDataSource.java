package com.example.filipemp.mapapp.data.remote.search;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.example.filipemp.mapapp.data.remote.commons.BaseRemoteDataSource;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by filipe.mp on 8/14/17.
 */

public class SearchRemoteDataSource extends BaseRemoteDataSource {

    private static final int MAX_RESULTS = 1;
    private Geocoder geoCoder;

    public interface SearchListener {
        void onSearchSuccess(LatLng latLng, String address);
    }

    private SearchListener searchListener;

    public SearchRemoteDataSource(Context context, FailureListener failureListener,
                                  SearchListener searchListener) {
        super(failureListener);
        this.searchListener = searchListener;
        geoCoder = new Geocoder(context, Locale.getDefault());
    }

    public void search(String query) {
        List<Address> addresses;
        try {
            addresses = geoCoder.getFromLocationName(query, MAX_RESULTS);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);

                searchListener.onSearchSuccess(
                        new LatLng(address.getLatitude(), address.getLongitude()),
                        address.getAddressLine(0));
            } else {
                failureListener.onFailure("Não foi possível encontrar o endereço buscado");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
