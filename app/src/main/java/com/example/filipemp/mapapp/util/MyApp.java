package com.example.filipemp.mapapp.util;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by filipe.mp on 8/17/17.
 */

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
