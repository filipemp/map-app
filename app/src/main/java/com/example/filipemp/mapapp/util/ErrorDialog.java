package com.example.filipemp.mapapp.util;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.example.filipemp.mapapp.R;

/**
 * Created by filipe.mp on 8/16/17.
 */

public class ErrorDialog extends DialogFragment {

    public static final String MESSAGE_KEY = "messageKey";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        String message = getString(R.string.error_dialog_generic_message);
        if (bundle != null) {
            message = bundle.getString(MESSAGE_KEY);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(message)
                .setNeutralButton(R.string.ok, null);

        return builder.create();
    }
}
