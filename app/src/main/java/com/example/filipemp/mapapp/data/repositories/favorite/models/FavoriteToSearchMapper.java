package com.example.filipemp.mapapp.data.repositories.favorite.models;

import com.example.filipemp.mapapp.data.remote.favorite.models.FavoriteResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by filipe.mp on 8/15/17.
 */

public class FavoriteToSearchMapper {

    public static List<SearchResult> map(List<FavoriteResponse> favoriteResponses) {
        List<SearchResult> results = new ArrayList<>();

        for (FavoriteResponse response : favoriteResponses) {
            results.add(map(response));
        }

        return results;
    }

    private static SearchResult map(FavoriteResponse favoriteResponse) {
        return new SearchResult(favoriteResponse.getName(), favoriteResponse.getLatitude(),
                favoriteResponse.getLongitude(), true);
    }
}
