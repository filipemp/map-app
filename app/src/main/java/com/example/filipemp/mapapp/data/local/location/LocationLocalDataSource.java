package com.example.filipemp.mapapp.data.local.location;

import android.content.Context;
import android.location.Location;

import com.github.pwittchen.prefser.library.rx2.Prefser;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by filipe.mp on 8/14/17.
 */

public class LocationLocalDataSource {
    private Context context;
    private Prefser prefser;

    private Location lastKnownLocation;
    private CameraPosition cameraPosition;

    private static final String LAST_LOCATION_KNOWN_KEY = "lastLocationKnownKey";
    private static final String CAMERA_POSITION_KEY = "cameraPositionKey";

    public LocationLocalDataSource(Context context) {
        this.context = context;
        setup();
    }

    private void setup() {
        prefser = new Prefser(context);
    }

    public void setLastKnownLocation(Location lastKnownLocation) {
        this.lastKnownLocation = lastKnownLocation;
    }

    public void setLastKnownLocation(double latitude, double longitude) {
        if (lastKnownLocation == null) {
            lastKnownLocation = new Location("user search");
        }
        lastKnownLocation.setLatitude(latitude);
        lastKnownLocation.setLongitude(longitude);
    }

    public Location getLastKnownLocation() {
        return lastKnownLocation;
    }

    public LatLng getLastKnownLocationLatLng() {
        if (lastKnownLocation == null) {
            return null;
        }
        return new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
    }

    public void setCameraPosition(CameraPosition cameraPosition) {
        this.cameraPosition = cameraPosition;
    }

    public CameraPosition getCameraPosition() {
        return cameraPosition;
    }

    public void saveData() {
        prefser.put(LAST_LOCATION_KNOWN_KEY, lastKnownLocation);
        prefser.put(CAMERA_POSITION_KEY, cameraPosition);
    }

    public void retrieveData() {
        lastKnownLocation = prefser.get(LAST_LOCATION_KNOWN_KEY, Location.class, null);
        cameraPosition = prefser.get(CAMERA_POSITION_KEY, CameraPosition.class, null);
    }
}
