package com.example.filipemp.mapapp.util;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.filipemp.mapapp.R;

/**
 * Created by filipe.mp on 8/16/17.
 */

public class AddFavoriteDialog extends DialogFragment {

    public interface OnOkClickListener {
        void onOkClickListener(DialogInterface dialog, String editTextValue);
    }

    private OnOkClickListener onOkClickListener;

    public void setOnOkClickListener(OnOkClickListener onOkClickListener) {
        this.onOkClickListener = onOkClickListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View rootCustomView = inflater.inflate(R.layout.dialog_field, null);
        EditText editText = (EditText) rootCustomView.findViewById(R.id.favorite_name_edit_text);

        builder.setMessage(R.string.add_favorite_dialog_message)
                .setView(rootCustomView)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    onOkClickListener.onOkClickListener(dialog, editText.getText().toString());
                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> AddFavoriteDialog.this.getDialog().cancel());

        return builder.create();
    }
}
