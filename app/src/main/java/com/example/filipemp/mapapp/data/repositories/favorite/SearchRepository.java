package com.example.filipemp.mapapp.data.repositories.favorite;

import android.content.Context;

import com.example.filipemp.mapapp.data.local.favorites.FavoriteLocalDataSource;
import com.example.filipemp.mapapp.data.remote.search.SearchRemoteDataSource;
import com.example.filipemp.mapapp.data.remote.commons.BaseRemoteDataSource;
import com.example.filipemp.mapapp.data.remote.favorite.FavoriteRemoteDataSource;
import com.example.filipemp.mapapp.data.remote.favorite.models.FavoriteResponse;
import com.example.filipemp.mapapp.data.repositories.favorite.models.FavoriteToSearchMapper;
import com.example.filipemp.mapapp.data.repositories.favorite.models.LatLngToSearchMapper;
import com.example.filipemp.mapapp.data.repositories.favorite.models.SearchResult;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by filipe.mp on 8/15/17.
 */

public class SearchRepository implements BaseRemoteDataSource.FailureListener,
        FavoriteRemoteDataSource.FavoriteListener,
        SearchRemoteDataSource.SearchListener,
        FavoriteLocalDataSource.Listener {

    public interface FavoriteRepositoryListener {
        void onFailure(String message);
        void onFavoriteSuccess(List<SearchResult> favorites);
        void onSearchSuccess(SearchResult result);
    }

    private FavoriteRepositoryListener listener;

    private FavoriteRemoteDataSource favoriteRemoteDataSource;
    private SearchRemoteDataSource searchRemoteDataSource;
    private FavoriteLocalDataSource favoriteLocalDataSource;

    private List<SearchResult> currentResults;

    public SearchRepository(Context context, FavoriteRepositoryListener favoriteListener) {
        this.listener = favoriteListener;
        favoriteRemoteDataSource = new FavoriteRemoteDataSource(this, this);
        searchRemoteDataSource = new SearchRemoteDataSource(context, this, this);
        favoriteLocalDataSource = new FavoriteLocalDataSource(this);
    }

    public void getFavorites() {
        favoriteRemoteDataSource.getFavorites();
    }

    public void search(String query) {
        searchRemoteDataSource.search(query);
    }

    public void save(SearchResult result) {
        favoriteLocalDataSource.save(result);
    }

    public void remove(SearchResult result) {
        favoriteLocalDataSource.remove(result);
    }

    @Override
    public void onFailure(String message) {
        listener.onFailure(message);
    }

    @Override
    public void onSuccess(List<FavoriteResponse> favorites) {
        currentResults = FavoriteToSearchMapper.map(favorites);
        favoriteLocalDataSource.getAll();
    }

    @Override
    public void onSearchSuccess(LatLng latLng, String address) {
        listener.onSearchSuccess(LatLngToSearchMapper.map(latLng, address));
    }

    @Override
    public void onFavoritesRetrieved(List<SearchResult> results) {
        currentResults.addAll(results);
        listener.onFavoriteSuccess(currentResults);
    }
}
