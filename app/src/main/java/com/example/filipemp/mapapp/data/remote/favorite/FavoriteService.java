package com.example.filipemp.mapapp.data.remote.favorite;

import com.example.filipemp.mapapp.data.remote.favorite.models.FavoritesResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by filipe.mp on 8/15/17.
 */

public interface FavoriteService {

    @GET("/favorites")
    Call<FavoritesResponse> getFavorites();
}
