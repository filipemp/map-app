package com.example.filipemp.mapapp.ui;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.filipemp.mapapp.R;
import com.example.filipemp.mapapp.data.local.location.LocationLocalDataSource;
import com.example.filipemp.mapapp.data.remote.commons.BaseRemoteDataSource;
import com.example.filipemp.mapapp.data.repositories.favorite.SearchRepository;
import com.example.filipemp.mapapp.data.repositories.favorite.models.SearchResult;
import com.example.filipemp.mapapp.util.DialogFactory;
import com.example.filipemp.mapapp.util.LatLngHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Mostly extracted from
 * https://github.com/googlemaps/android-samples/tree/master/tutorials/CurrentPlaceDetailsOnMap
 * */
public class MapsActivity extends AppCompatActivity
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        BaseRemoteDataSource.FailureListener,
        SearchRepository.FavoriteRepositoryListener,
        ResultsAdapter.ResultsAdapterListener {

    private static final String TAG = MapsActivity.class.getSimpleName();
    private GoogleMap map;
    private Marker centerMarker;

    private GoogleApiClient googleApiClient;

    private final LatLng defaultLocation = LatLngHelper.getDefaultLatLng();
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean locationPermissionGranted;

    private LocationLocalDataSource locationLocalDataSource;
    private SearchRepository searchRepository;

    private List<SearchResult> results;
    private RecyclerView resultsRecyclerView;
    private ResultsAdapter resultsAdapter;

    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        results = new ArrayList<>();

        resultsAdapter = new ResultsAdapter(results, this);

        resultsRecyclerView = (RecyclerView) findViewById(R.id.results_list);
        resultsRecyclerView.setAdapter(resultsAdapter);
        resultsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        resultsRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext()));

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();

        locationLocalDataSource = new LocationLocalDataSource(this);
        searchRepository = new SearchRepository(this, this);
        searchRepository.getFavorites();
    }

    @Override
    protected void onStart() {
        super.onStart();

        locationLocalDataSource.retrieveData();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (map != null && centerMarker != null) {
            locationLocalDataSource.setCameraPosition(map.getCameraPosition());
            locationLocalDataSource.setLastKnownLocation(centerMarker.getPosition().latitude,
                    centerMarker.getPosition().longitude);

            map.clear();
            centerMarker = null;

            locationLocalDataSource.saveData();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchRepository.search(query);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        searchView.setQueryHint(getString(R.string.search_hint));

        searchView.setOnQueryTextFocusChangeListener(this::onSearchViewFocusChange);

        return true;
    }

    /**
     * Builds the map when the Google Play services client is successfully connected.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Build the map.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Handles failure to connect to the Google Play services client.
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        // Refer to the reference doc for ConnectionResult to see what error codes might
        // be returned in onConnectionFailed.
        Log.d(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    /**
     * Handles suspension of the connection to the Google Play services client.
     */
    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "Play services connection suspended");
    }

    /**
     * Manipulates the map when it's available.
     * This callback is triggered when the map is ready to be used.
     */
    @Override
    public void onMapReady(GoogleMap map) {
        this.map = map;

        // Use a custom info window adapter to handle multiple lines of text in the
        // info window contents.
        this.map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            // Return null here, so that getInfoContents() is called next.
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater().inflate(R.layout.custom_info_contents,
                        (FrameLayout)findViewById(R.id.map), false);

                TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                title.setText(marker.getTitle());

                TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                snippet.setText(marker.getSnippet());

                return infoWindow;
            }
        });

        if (locationLocalDataSource.getLastKnownLocation() == null && locationLocalDataSource.getCameraPosition() == null) {
            // Turn on the My Location layer and the related control on the map.
            updateLocationUI();

            // Get the current location of the device and set the position of the map.
            getDeviceLocation();
        } else {
            updateCenterMarker();
        }
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private void getDeviceLocation() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        if (locationPermissionGranted) {
            Location location = LocationServices.FusedLocationApi
                    .getLastLocation(googleApiClient);
            locationLocalDataSource.setLastKnownLocation(location);
        }

        // Set the map's camera position to the current location of the device.
        if (locationLocalDataSource.getCameraPosition() != null) {
            map.moveCamera(CameraUpdateFactory.newCameraPosition(locationLocalDataSource.getCameraPosition()));
        } else if (locationLocalDataSource.getLastKnownLocation() != null) {
            updateCameraPosition();
        } else {
            Log.d(TAG, "Current location is null. Using defaults.");
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, DEFAULT_ZOOM));
            map.getUiSettings().setMyLocationButtonEnabled(false);
        }

        addCenterMarker();
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        locationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private void updateLocationUI() {
        if (map == null) {
            return;
        }

        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        if (locationPermissionGranted) {
            map.setMyLocationEnabled(true);
        } else {
            map.setMyLocationEnabled(false);
            locationLocalDataSource.setLastKnownLocation(null);
        }
    }

    private void addCenterMarker() {
        centerMarker = map.addMarker(new MarkerOptions().position(getCurrentLatLng()));
        updateCameraPosition();
        setupCenterMarkerLogic();
    }

    private void setupCenterMarkerLogic() {
        map.setOnCameraMoveListener(() ->
            centerMarker.setPosition(map.getCameraPosition().target)
        );
    }

    private LatLng getCurrentLatLng() {
        if (locationLocalDataSource.getLastKnownLocation() != null) {
            return locationLocalDataSource.getLastKnownLocationLatLng();
        }

        return defaultLocation;
    }

    @Override
    public void onSearchSuccess(SearchResult result) {
        results.add(0, result);
        resultsAdapter.notifyDataSetChanged();
    }

    private void updateCenterMarker() {
        if (centerMarker != null) {
            centerMarker.setPosition(getCurrentLatLng());
            updateCameraPosition();
        } else {
            addCenterMarker();
        }
    }

    private void updateCameraPosition() {
        if (locationLocalDataSource.getLastKnownLocationLatLng() != null) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    locationLocalDataSource.getLastKnownLocationLatLng(), DEFAULT_ZOOM));
        }
    }

    @Override
    public void onFailure(String message) {
        DialogFragment dialogFragment = DialogFactory
                .buildDialog(DialogFactory.DialogType.ERROR, null, message);
        showDialog(dialogFragment);
        Log.e("Fail", message);
    }

    @Override
    public void onFavoriteSuccess(List<SearchResult> favorites) {
        results.addAll(favorites);
        resultsAdapter.notifyDataSetChanged();
    }

    private void onSearchViewFocusChange(View view, boolean hasFocus) {
        if (hasFocus) {
            resultsRecyclerView.setVisibility(View.VISIBLE);
        } else {
            hideResults();
        }
    }

    @Override
    public void onItemClicked(LatLng position) {
        searchView.clearFocus();
        locationLocalDataSource.setLastKnownLocation(position.latitude, position.longitude);
        updateCenterMarker();
        hideResults();
    }

    @Override
    public void onStarClicked(DialogFragment dialogFragment) {
        showDialog(dialogFragment);
    }

    @Override
    public void onRemoveItem(SearchResult result) {
        results.remove(result);
        resultsAdapter.notifyDataSetChanged();
        searchRepository.remove(result);
    }

    @Override
    public void onSaveItem(SearchResult result) {
        searchRepository.save(result);
        resultsAdapter.notifyDataSetChanged();
    }

    private void hideResults() {
        resultsRecyclerView.setVisibility(View.GONE);
    }

    private void showDialog(DialogFragment dialogFragment) {
        FragmentManager fm = getSupportFragmentManager();
        dialogFragment.show(fm, "dialog");
    }
}
