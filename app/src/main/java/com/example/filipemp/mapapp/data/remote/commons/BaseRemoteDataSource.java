package com.example.filipemp.mapapp.data.remote.commons;

/**
 * Created by filipe.mp on 8/15/17.
 */

public class BaseRemoteDataSource {

    public interface FailureListener {
        void onFailure(String message);
    }

    protected FailureListener failureListener;

    public BaseRemoteDataSource(FailureListener failureListener) {
        this.failureListener = failureListener;
    }
}
