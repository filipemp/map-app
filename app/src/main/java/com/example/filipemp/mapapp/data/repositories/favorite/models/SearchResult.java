package com.example.filipemp.mapapp.data.repositories.favorite.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by filipe.mp on 8/15/17.
 */

public class SearchResult extends RealmObject {

    @PrimaryKey
    private String id;
    private String name;
    private Double latitude;
    private Double longitude;
    private boolean isFavorite;

    public SearchResult() {}

    public SearchResult(String name, Double latitude, Double longitude, boolean isFavorite) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isFavorite = isFavorite;
    }

    public String getName() {
        return name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
