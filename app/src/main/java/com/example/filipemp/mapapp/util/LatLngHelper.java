package com.example.filipemp.mapapp.util;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by filipe.mp on 8/14/17.
 */

public class LatLngHelper {

    public static final LatLng getDefaultLatLng() {
        return new LatLng(-33.8523341, 151.2106085);
    }
}
