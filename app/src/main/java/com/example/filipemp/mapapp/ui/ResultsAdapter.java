package com.example.filipemp.mapapp.ui;

import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.filipemp.mapapp.R;
import com.example.filipemp.mapapp.data.repositories.favorite.models.SearchResult;
import com.example.filipemp.mapapp.util.DialogFactory;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by filipe.mp on 8/16/17.
 */

public class ResultsAdapter extends RecyclerView.Adapter<ResultViewHolder> implements
        ResultViewHolder.ViewHolderListener {

    public interface ResultsAdapterListener {
        void onItemClicked(LatLng position);
        void onStarClicked(DialogFragment dialogFragment);
        void onSaveItem(SearchResult result);
        void onRemoveItem(SearchResult result);
    }

    private List<SearchResult> results;
    private ResultsAdapterListener listener;

    public ResultsAdapter(List<SearchResult> results, ResultsAdapterListener listener) {
        this.results = results;
        this.listener = listener;
    }

    @Override
    public ResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View genreItemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_result_item, parent, false);

        return new ResultViewHolder(genreItemView, this);
    }

    @Override
    public void onBindViewHolder(ResultViewHolder holder, int position) {
        SearchResult result = results.get(position);

        holder.bind(result, position);
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    @Override
    public void onStarClicked(int position, ResultViewHolder resultViewHolder) {
        DialogFragment dialogFragment = DialogFactory
                .buildDialog(DialogFactory.DialogType.ADD_FAVORITE, (dialog, name) -> {
                    SearchResult result = results.get(position);
                    result.setFavorite(!result.isFavorite());
                    resultViewHolder.updateStarState();
                    result.setName(name);
                    listener.onSaveItem(result);
        }, null);

        if (!results.get(position).isFavorite()) {
            listener.onStarClicked(dialogFragment);
        } else {
            results.get(position).setFavorite(!results.get(position).isFavorite());
            resultViewHolder.updateStarState();
            listener.onRemoveItem(results.get(position));
        }
    }

    @Override
    public void onItemClicked(int position) {
        listener.onItemClicked(new LatLng(results.get(position).getLatitude(),
                results.get(position).getLongitude()));
    }
}
