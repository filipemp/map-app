package com.example.filipemp.mapapp.util;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by filipe.mp on 8/16/17.
 */

public class DialogFactory {
    public enum DialogType {
        ERROR, ADD_FAVORITE
    }

    public static DialogFragment buildDialog(DialogType type,
                                             AddFavoriteDialog.OnOkClickListener onOkClickListener,
                                             String message) {
        DialogFragment dialogFragment = null;

        if (type == DialogType.ADD_FAVORITE) {
            AddFavoriteDialog dialog = new AddFavoriteDialog();
            dialog.setOnOkClickListener(onOkClickListener);
            dialogFragment = dialog;
        } else if (type == DialogType.ERROR) {
            dialogFragment = new ErrorDialog();
            Bundle args = new Bundle();
            args.putString(ErrorDialog.MESSAGE_KEY, message);
            dialogFragment.setArguments(args);
        }

        return dialogFragment;
    }
}
