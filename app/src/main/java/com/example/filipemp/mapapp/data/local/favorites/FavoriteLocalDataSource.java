package com.example.filipemp.mapapp.data.local.favorites;

import com.example.filipemp.mapapp.data.repositories.favorite.models.SearchResult;

import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by filipe.mp on 8/17/17.
 */

public class FavoriteLocalDataSource {

    private Realm realm;
    private RealmResults<SearchResult> realmResults;

    public interface Listener {
        void onFavoritesRetrieved(List<SearchResult> results);
    }

    private Listener listener;

    public FavoriteLocalDataSource(Listener listener) {
        realm = Realm.getDefaultInstance();
        this.listener = listener;
    }

    public void save(SearchResult result) {
        String name = result.getName();
        double latitude = result.getLatitude();
        double longitude = result.getLongitude();
        boolean isFavorite = result.isFavorite();

        realm.executeTransactionAsync(
                realm1 -> {
                    SearchResult newResult = realm1.createObject(SearchResult.class, UUID.randomUUID().toString());
                    newResult.setName(name);
                    newResult.setLatitude(latitude);
                    newResult.setLongitude(longitude);
                    newResult.setFavorite(isFavorite);
                },
                () -> {},
                error -> error.printStackTrace());
    }

    public void remove(SearchResult result) {

        realm.executeTransactionAsync(realm1 -> {
            realmResults = realm1.where(SearchResult.class)
                    .equalTo("id", result.getId())
                    .findAll();

            if (realmResults.size() > 0) {
                realmResults.deleteFirstFromRealm();
            }
        });
    }

    public void getAll() {
        realmResults = realm.where(SearchResult.class)
                .findAllAsync();
        realmResults.addChangeListener(searchResults -> {
            listener.onFavoritesRetrieved(realm.copyFromRealm(searchResults));
            searchResults.removeAllChangeListeners();
        });
    }
}
